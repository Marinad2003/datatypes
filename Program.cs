﻿using System;

namespace DataTypes_Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task1();
            //Task2();
            //Task3();
            //Task4();
            //Task5();
            Task6();

        }

        static void Task1()
        {
            int value1 = 3;
            int value2 = 4;
            int value3 = 0;
            value3 = value1;
            value1 = value2;
            value2 = value3;
            Console.WriteLine(" " + value1 + " " + value2);
        }

        static void Task2()
        {
            int var1 = 20;
            int var2 = 30;
            var1 = var1 * 3;
            var2 = var2 + 12;
            Console.WriteLine(var1.ToString() + " " + var2.ToString());


        }
        static void Task3()
        {
            int milk = 50;
            int matches = 20;
            int amountMatches = 120;
            int amountMilk = 210;
            int sum = milk * amountMilk + matches * amountMatches;

            Console.WriteLine(" " + sum);
        }
        static void Task4()
        {
            int value1 = Convert.ToInt32(Console.ReadLine());
            int value2 = Convert.ToInt32(Console.ReadLine());
            int sum = value1 + value2;
            Console.WriteLine("Summ is: " + sum);
            int mult = value1 * value2;
            Console.WriteLine("Multiplication is: " + mult);
            int diff = value1 - value2;
            Console.WriteLine("Difference is: " + diff);

            // *multiply /divide ^2power  +sum  -difference  =equal
        }
        static void Task5()
        {
            float q = 20.1f;
            float w = 5.3f;
            Console.WriteLine(q / w);

        }
        static void Task6()
        {
            int n = 37;
            int z = n % 10;
            int y = n / 10;
            Console.WriteLine(z + y);            
        }

    }
}

